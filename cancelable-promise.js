// Доброго дня.
// Ні коли не покривав тестами програмний код, 
//  у MapInfo це було не можливо, а у WEB GIS до цёго ще не дійшли.
// Але Я розібрався у test як він працює та що він робить,
//  на далі вже можу самостійно покривати тестами проєкт, за це дякую Вам велике.

// У package.json зробив заміну
// "test": "echo \"Error: no test specified\" && exit 1"
// на
// "test": "jest"

// Тест запрацював, але при об’яві звичайного класу він свариться 
// на «Jest encountered an unexpected token». 
// Про token знання є, але замало практики((.
// Як Я зрозумів що Ви Мені надали маленьку частинку загального коду, 
//  на далі його доповнювати займе багато часу, а це не вигідно ні Вам ні Мені.
// Для відміни дій у ланцюгу завдань Я навів два приклади, 
// один через змінну інший на пряму, обидва через перевірку на вірне відпрацювання.

// Готовий і на далі розвиватися у цёму напрямку.

export default class CancelablePromise {
    constructor(name) {
        this.name = name;
    }
}

const testClass = new CancelablePromise();
try {
    if (testClass !== new CancelablePromise()) {
        throw new SyntaxError("Data is incorrect");
    } else {
        const jobClass = testClass;
        jobClass();
    }
} catch (e) {
    e();
}

try {
    const jobClass = new CancelablePromise();
    jobClass();
} catch (e) {
    const alert = "Data is incorrect";
    alert();
}

// class CancelablePromise extends Error {
//     printCustomerMessage() {
//       return `Verification failed :-( (${this.message})`;
//     }
// }
  
// try {
//     throw new CancelablePromise();
// } catch (error) {
//     console.log('Data is incorrect', error);
//     throw error;
// }
